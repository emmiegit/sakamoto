#
# command.py
#
# sakamoto - A Discord bot to manage your status.
# Copyright (c) 2016 Ammon Smith and Bradley Cai
#
# sakamoto is available free of charge under the terms of the MIT
# License. You are free to redistribute and/or modify it under those
# terms. It is distributed in the hopes that it will be useful, but
# WITHOUT ANY WARRNARTY. See the LICENSE file for more details.
#

__all__ = [
    "HELP_STRING",
    "init_channel",
    "close_channel",
    "update_status",
    "delete_status",
    "purge_statuses",
    "add_fake_status",
    "print_uptime",
    "print_help",
    "write_bot_message",
    "clear_bot_message",
]

from dummy_user import DummyUser
from util import *
import asyncio
import discord

HELP_STRING = \
"""
_General:_
`!delete` - Remove your status from the list.
`!update` - Force an update to the status message.
`!uptime` - Print how long I've been running.
`!clear` - Clear this message area.
`!help` - Print this help message.

_Administrative:_
`!listen` - Start listening on this channel for status updates.
`!stop` - Stop listening on this channel and delete messages.
`!set [user] [message]` - Manually set a status, even for a nonexistant user.
`!purge [user1] [user2]` Delete other users' statuses. If no users are
specified, then delete all statuses.
"""

@asyncio.coroutine
def init_channel(bot, message):
    if message.channel not in bot.state.channels.keys():
        bot.logger.info("Initializing %s at request of %s." %
                (get_server_and_channel_name(message), get_user_name(message.author)))

        # Create the messages
        try:
            status_message = yield from bot.send_message(message.channel, "Loading...")
            bot_message = yield from bot.send_message(message.channel, ".")
        except:
            bot.logger.error("Unable to send messages: %s")
            return

        bot.state.channels[message.channel] = status_message, bot_message, {}
        yield from bot.edit_message(status_message, bot.state.get_state_string(message.channel))

        # Pin the status message
        try:
            bot.pin_message(status_message)
        except discord.Forbidden:
            bot.logger.info("Unable to pin status message on %s." %
                    get_server_and_channel_name(message))
            write_bot_message(bot, message.channel,
                    "I can't pin the status message. Can you check my permissions?")
        except:
            bot.logger.error("Unable to pin status message on %s." %
                    get_server_and_channel_name(message))
    else:
        bot.logger.info("%s wants to initialize a channel I'm already listening to." %
                get_user_name(message.author))

        bot.state.annoying += 1
        if bot.state.annoying >= 5:
            yield from write_bot_message(bot, message.channel, "Shut up!")
            bot.state.annoying = 1
        else:
            yield from write_bot_message(bot, message.channel, "I'm already listening!")

@asyncio.coroutine
def close_channel(bot, message):
    if message.channel in bot.state.channels.keys():
        bot.logger.info("Closing %s at request of %s." %
                (get_server_and_channel_name(message), get_user_name(message.author)))
        status_message, bot_message, users = bot.state.channels[message.channel]

        try:
            yield from bot.delete_message(status_message)
        except:
            bot.logger.error("Unable to clean up status message.")

        try:
            yield from bot.delete_message(bot_message)
        except:
            bot.logger.error("Unable to clean up bot message.")

        try:
            yield from bot.delete_message(message)
        except:
            bot.logger.error("Unable to clean up command message.")

        del bot.state.channels[message.channel]
    else:
        bot.logger.info("%s is not a status channel." % get_server_and_channel_name(message))

@asyncio.coroutine
def update_status(bot, message):
    bot.logger.info("Updating status of %s." % get_server_and_channel_name(message))
    status_message = bot.state.get_status_message(message.channel)
    yield from bot.edit_message(status_message, bot.state.get_state_string(message.channel))

@asyncio.coroutine
def delete_status(bot, message):
    bot.logger.info("%s is attempting to self-remove from %s." %
            (get_user_name(message.author), get_server_and_channel_name(message)))

    if not bot.state.delete_status(message.channel, message.author):
        bot.logger.info("\"%s\" does not have a status here!" % username)
        yield from write_bot_message(bot, message.channel, "User %s does not appear to have a status." % username)

    # Update status
    yield from update_status(bot, message)

@asyncio.coroutine
def purge_statuses(bot, message):
    bot.logger.info("Attempting to purge \"%s\" from %s." %
            (get_user_name(message.author), get_server_and_channel_name(message)))

    parts = message.content.split(" ")
    if len(parts) < 2:
        bot.state.delete_all_statuses(message.channel)

    names = parts[1:]
    couldnt_find = []
    for name in names:
        user = bot.state.get_user(message.channel, name)
        if user:
            bot.logger.info("Purging %s from %s." % (name, get_server_and_channel_name(message)))
            bot.state.delete_status(message.channel, user)
        else:
            couldnt_find.append(name)

    if couldnt_find:
        bot.logger.info("Unable to purge from %s: %s." %
                (get_server_and_channel_name(message), ", ".join(couldnt_find)))
        yield from write_bot_message(bot, message.channel, "Couldn't find %s." % ", ".join(couldnt_find))

    status_message = bot.state.get_status_message(message.channel)
    yield from bot.edit_message(status_message, bot.state.get_state_string(message.channel))

@asyncio.coroutine
def add_fake_status(bot, message):
    bot.logger.info("Adding fake status \"%s\" on %s." %
            (message.content, get_server_and_channel_name(message)))

    parts = message.content.split(" ")
    if len(parts) < 2:
        yield from write_bot_message(bot, message.channel, "Not enough arguments.")
        return

    name = parts[1]
    status = " ".join(parts[2:])
    member = message.server.get_member_named(name)
    if member is None:
        member = DummyUser(name)

    bot.state.update_status(message.channel, member, status)
    status_message = bot.state.get_status_message(message.channel)
    yield from bot.edit_message(status_message, bot.state.get_state_string(message.channel))

@asyncio.coroutine
def print_uptime(bot, channel):
    bot.logger.info("Printing uptime on %s." % get_server_and_channel_name2(channel))
    if channel not in bot.state.channels.keys():
        yield from bot.send_message(message, channel, bot.state.get_uptime_string())

    bot_message = bot.state.get_bot_message(channel)
    yield from bot.edit_message(bot_message, bot.state.get_uptime_string(bot))

@asyncio.coroutine
def print_help(bot, channel):
    bot.logger.info("Printing help on %s." % get_server_and_channel_name2(channel))
    if channel not in bot.state.channels.keys():
        yield from bot.send_message(message, channel, HELP_STRING)
        return

    bot_message = bot.state.get_bot_message(channel)
    yield from bot.edit_message(bot_message, HELP_STRING)

@asyncio.coroutine
def write_bot_message(bot, channel, text):
    bot.logger.info("Printing bot message on %s." % get_server_and_channel_name2(channel))
    bot.logger.info(str(text))
    bot_message = bot.state.get_bot_message(channel)
    yield from bot.edit_message(bot_message, "~\n%s" % text)

@asyncio.coroutine
def clear_bot_message(bot, channel):
    bot.logger.info("Clearing bot message on %s." % get_server_and_channel_name2(channel))
    bot_message = bot.state.get_bot_message(channel)
    yield from bot.edit_message(bot_message, ".")

