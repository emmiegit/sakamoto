#
# state.py
#
# sakamoto - A Discord bot to manage your status.
# Copyright (c) 2016 Ammon Smith and Bradley Cai
#
# sakamoto is available free of charge under the terms of the MIT
# License. You are free to redistribute and/or modify it under those
# terms. It is distributed in the hopes that it will be useful, but
# WITHOUT ANY WARRNARTY. See the LICENSE file for more details.
#

__all__ = [
    "STATUS_TIME_FORMAT",
    "SECONDS_IN_A_MINUTE",
    "SECONDS_IN_AN_HOUR",
    "SECONDS_IN_A_DAY",
    "SECONDS_IN_A_MONTH",
    "SECONDS_IN_A_YEAR",
    "BotState",
]

from datetime import datetime
from util import *
import asyncio
import time

STATUS_TIME_FORMAT = "%I:%M %p"

SECONDS_IN_A_MINUTE = 60
SECONDS_IN_AN_HOUR = 60 * SECONDS_IN_A_MINUTE
SECONDS_IN_A_DAY = 24 * SECONDS_IN_AN_HOUR
SECONDS_IN_A_MONTH = 30 * SECONDS_IN_A_DAY
SECONDS_IN_A_YEAR = 365 * SECONDS_IN_A_DAY

class BotState(object):
    def __init__(self):
        self.start_time = datetime.now()
        # { discord.Channel : (discord.Message, discord.Message, users) }
        # where the "users" dict is: { discord.User : (status, last_updated) }
        self.channels = {}
        self.annoying = 1

    def get_status_message(self, channel):
        return self.channels[channel][0]

    def get_bot_message(self, channel):
        return self.channels[channel][1]

    def get_users(self, channel):
        return self.channels[channel][2]

    def get_user(self, channel, username):
        username = username.lower()

        for user in self.get_users(channel).keys():
            if user.name.lower() == username or \
                (user.nick and user.nick.lower() == username):
                return user
        return None

    def update_status(self, channel, discord_user, status):
        status = status.replace("\n", " ")
        self.get_users(channel)[discord_user] = (status, time.time())

    def delete_status(self, channel, discord_user):
        users = self.get_users(channel)
        for user in users.keys():
            if user == discord_user:
                del users[discord_user]
                return True
        return False

    def delete_all_statuses(self, channel):
        self.get_users(channel).clear()

    def get_state_string(self, channel):
        lines = ["**Statuses:**"]
        users = self.get_users(channel)

        if not users:
            lines.append("(none)")

        for discord_user, data in users.items():
            lines.append("%s: %s (last updated %s)" %
                    (get_user_name(discord_user), data[0], self.format_time(data[1], STATUS_TIME_FORMAT)))

        return "\n".join(lines)

    def format_time(self, seconds, time_format):
        st_time = time.localtime(seconds)
        return time.strftime(time_format, st_time)

    def get_uptime(self):
        return time.perf_counter() - self.start_time

    def get_uptime_string(self, bot):
        seconds = int((datetime.now() - self.start_time).seconds)
        bot.logger.debug("Uptime: %d seconds." % seconds)
        parts = []

        if seconds >= SECONDS_IN_A_YEAR:
            parts.append("%d year%s" %
                    plural(seconds / SECONDS_IN_A_YEAR))
            seconds %= SECONDS_IN_A_YEAR
            bot.logger.debug("year, remaining seconds: %d" % seconds)

        if seconds >= SECONDS_IN_A_MONTH:
            parts.append("%d month%s" %
                    plural(seconds / SECONDS_IN_A_MONTH))
            seconds %= SECONDS_IN_A_MONTH
            bot.logger.debug("month, remaining seconds: %d" % seconds)

        if seconds >= SECONDS_IN_A_DAY:
            parts.append("%d day%s" %
                    plural(seconds / SECONDS_IN_A_DAY))
            seconds %= SECONDS_IN_A_DAY
            bot.logger.debug("day, remaining seconds: %d" % seconds)

        if seconds >= SECONDS_IN_AN_HOUR:
            parts.append("%d hour%s" %
                    plural(seconds / SECONDS_IN_AN_HOUR))
            seconds %= SECONDS_IN_AN_HOUR
            bot.logger.debug("hour, remaining seconds: %d" % seconds)

        if seconds >= SECONDS_IN_A_MINUTE:
            parts.append("%d minute%s" %
                    plural(seconds / SECONDS_IN_A_MINUTE))
            seconds %= SECONDS_IN_A_MINUTE
            bot.logger.debug("minute, remaining seconds: %d" % seconds)

        parts.append("%d second%s" % plural(seconds))
        uptime_string = ", ".join(parts)

        bot.logger.info("Calculated uptime string: %s." % uptime_string)
        return "I've been up for %s now." % uptime_string

