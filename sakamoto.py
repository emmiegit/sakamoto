#!/usr/bin/env python3

#
# sakamoto.py
#
# sakamoto - A Discord bot to manage your status.
# Copyright (c) 2016 Ammon Smith and Bradley Cai
#
# sakamoto is available free of charge under the terms of the MIT
# License. You are free to redistribute and/or modify it under those
# terms. It is distributed in the hopes that it will be useful, but
# WITHOUT ANY WARRNARTY. See the LICENSE file for more details.
#

__all__ = [
    "LOG_TO_STDOUT",
    "LOG_FILE_MODE",
]

from command import *
from state import BotState
from util import *
import argparse
import asyncio
import auth
import discord
import logging

LOG_FILE_MODE = "w"

if __name__ == "__main__":
    argparser = argparse.ArgumentParser(description="Host a Sakamoto status server.")
    argparser.add_argument('-n', '--no-stdout',
            dest='stdout', action='store_false',
            help="Don't output to stdout.")
    args = argparser.parse_args()

    # Set up logging
    logger = logging.getLogger('discord')
    logger.setLevel(level=logging.INFO)

    log_handler = logging.FileHandler(filename='bot.log', encoding='utf-8', mode=LOG_FILE_MODE)
    log_handler.setFormatter(logging.Formatter(
        "%(asctime)s [%(levelname)s] %(name)s: %(message)s", datefmt="[%d/%m/%Y %H:%M]"))
    logger.addHandler(log_handler)

    if args.stdout:
        log_handler = logging.FileHandler(filename='/dev/stdout', encoding='utf-8', mode=LOG_FILE_MODE)
        log_handler.setFormatter(logging.Formatter(
            "%(asctime)s [%(levelname)s] %(name)s: %(message)s", datefmt="[%d/%m/%Y %H:%M]"))
        logger.addHandler(log_handler)

    # Open client
    bot = discord.Client()
    bot.logger = logger
    bot.state = BotState()

    # Create event handlers
    @bot.async_event
    def on_ready():
        # Print welcome state
        users = len(set(bot.get_all_members()))
        servers = len(bot.servers)
        channels = len([c for c in bot.get_all_channels()])
        logger.info("")
        logger.info("%s is now online." % bot.user.name)
        logger.info("")
        logger.info("Connected to:")
        logger.info("* %d server%s" % plural(servers))
        logger.info("* %d channel%s" % plural(channels))
        logger.info("* %d user%s" % plural(users))
        logger.info("")
        logger.info("Ready~~")

        # Set default settings
        yield from bot.change_status(game=discord.Game(name="with his tail"))


    @bot.async_event
    def on_message(message):
        if message.author == bot.user or type(message.channel) == discord.PrivateChannel:
            return

        # Status channel management
        if message.content == "!listen":
            yield from init_channel(bot, message)
        elif message.content == "!stop":
            yield from close_channel(bot, message)
            return
        elif message.channel not in bot.state.channels.keys():
            # Channel is not initialized, do nothing
            return
        elif message.content.startswith("!set"):
            yield from add_fake_status(bot, message)
        elif message.content == "!delete":
            yield from delete_status(bot, message)
        elif message.content.startswith("!purge"):
            yield from purge_statuses(bot, message)
        elif message.content == "!update":
            yield from update_status(bot, message)
            yield from write_bot_message(bot, message.channel, "Updated status message.")
        elif message.content == "!uptime":
            yield from print_uptime(bot, message.channel)
        elif message.content == "!help":
            yield from print_help(bot, message.channel)
        elif message.content == "!clear":
            yield from clear_bot_message(bot, message.channel)
        elif message.content.startswith("!"):
            bot.logger.info("Invalid command from %s: \"%s\"." % (get_user_name(message.author), message.content))
            yield from write_bot_message(bot, message.channel,
                    "Invalid command: `%s`.\nTry using `!help`." % message.content)
        else:
            # Change status
            bot.state.update_status(message.channel, message.author, message.content)

            status_message = bot.state.get_status_message(message.channel)
            yield from bot.edit_message(status_message, bot.state.get_state_string(message.channel))

        # Delete command message
        bot.logger.info("Deleting message id %s: \"%s\"." % (message.id, message.content))
        try:
            yield from bot.delete_message(message)
        except discord.Forbidden:
            yield from write_bot_message(bot, message.channel, "I can't delete that message. Can you check my permissions?")

    # Get the authentication token
    client_secret = auth.get_client_secret("client_secret.json")
    token = client_secret["bot"]["token"]
    del client_secret

    # Run the bot
    bot.run(token)

