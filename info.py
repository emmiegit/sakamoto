#
# info.py
#
# sakamoto - A Discord bot to manage your status.
# Copyright (c) 2016 Ammon Smith
#
# sakamoto is available free of charge under the terms of the MIT
# License. You are free to redistribute and/or modify it under those
# terms. It is distributed in the hopes that it will be useful, but
# WITHOUT ANY WARRNARTY. See the LICENSE file for more details.
#

PROGRAM_NAME = "sakamoto"
VERSION = "0.5"
URL = "https://gitlab.com/ammongit/sakamoto"

def get_user_agent():
    return "User-Agent: Sakamoto_DiscordBot (%s, %s)" % (URL, VERSION)

