#
# util.py
#
# sakamoto - A Discord bot to manage your status.
# Copyright (c) 2016 Ammon Smith and Bradley Cai
#
# sakamoto is available free of charge under the terms of the MIT
# License. You are free to redistribute and/or modify it under those
# terms. It is distributed in the hopes that it will be useful, but
# WITHOUT ANY WARRNARTY. See the LICENSE file for more details.
#

__all__ = [
    "plural",
    "get_user_name",
    "get_server_name",
    "get_server_and_channel_name",
    "get_server_and_channel_name2",
]

def plural(count):
    return count, ("" if count == 1 else "s")

def get_server_and_channel_name(discord_message):
    return "\"%s#%s\"" % \
        (get_server_name(discord_message.server), discord_message.channel.name)

def get_server_and_channel_name2(discord_channel):
    return "\"%s#%s\"" % \
        (get_server_name(discord_channel.server), discord_channel.name)

def get_server_name(discord_server):
    if discord_server:
        return discord_server.name
    else:
        return discord_server

def get_user_name(discord_user):
    if discord_user.nick:
        return discord_user.nick
    else:
        return discord_user.name

