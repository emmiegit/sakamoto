### Sakamoto
![](misc/sakamoto.png)

Sakamoto is a [Discord](https://discordapp.com) bot that maintains your status, whether it's "busy", "available" or anything in between.

This program is licensed under the MIT License. (See `LICENSE` for details).

#### Depedencies
Sakamoto uses [discord.py](https://github.com/Rapptz/discord.py) as its backend.
```
pip install --user --upgrade git+https://github.com/Rapptz/discord.py@async
```
(If you want to install the requirement globally, omit the `--user` flag and run as root)

#### Running
In order to run Sakamoto, you'll need to [create a Discord application](https://discordapp.com/developers/applications/me) and get your OAuth2 id and client secret.

Credentials should be stored in `client_secret.json`, in the following form:
```json
{
    "oauth2": {
        "id": "CLIENT_ID",
        "secret": "CLIENT_SECRET"
    },

    "bot": {
        "username": "BOT_USERNAME#1000",
        "bot-id": "100000000000000000",
        "token": "BOT_TOKEN"
    }
}
```

After satisfying all the dependencies, run sakamoto:
```bash
python3 sakamoto.py
```

#### Adding to a server
We are not currently hosting this bot for public usage.

To add your own running bot to a server, open the following URL:
<tt>https://discordapp.com/oauth2/authorize?&client\_id=**(YOUR CLIENT ID HERE)**&scope=bot&permissions=0</tt>

#### Using the bot
You should create a dedicated channel for the bot. You should create a custom role for the bot, and give it the "manage messages" permission for that channel only. You may also want to ban the bot from posting in other channels to prevent annoying people from using `!listen` in non-status channels.

