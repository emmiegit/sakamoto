#
# dummy_user.py
#
# sakamoto - A Discord bot to manage your status.
# Copyright (c) 2016 Ammon Smith and Bradley Cai
#
# sakamoto is available free of charge under the terms of the MIT
# License. You are free to redistribute and/or modify it under those
# terms. It is distributed in the hopes that it will be useful, but
# WITHOUT ANY WARRNARTY. See the LICENSE file for more details.
#

__all__ = [
    "DummyUser",
]

class DummyUser(object):
    def __init__(self, name, nick=None):
        self.name = name
        self.nick = nick

